/**
 * 全局框架
 * @type {{}}
 */
var PTFrame = {
        side : $('.side'),//侧边框架div
        log:function(){
                for(var i in arguments) {
                        console.log(arguments[i]);
                }
        },

        /**
         * 文档高度
         */
        DocumentH:0,
        DocumentW:0,
        MainFrameTable:'',
        init: function () {
                this.DocumentH = this.getDocumentH();
                this.DocumentW = this.getDocumentW();
                this.MainFrameTable = $('#MainFrameTable');
                this.SideLeft = $('.side-left');
                this.topHeader = $('.top_header');
                this.log('DocumentH=' + this.DocumentH);
                this.log('topH=' + this.topHeader.height());
                this.setHeight();
        },
        //获取文档高度
        getDocumentH: function () {
                return $(window).height();
        },
        getDocumentW: function () {
                return $(window).width();
        },

        onResize:function() {
                this.setHeight();
        },
        setHeight:function() {
                var MainFrameHeight=this.getDocumentH()-this.topHeader.height();
                this.MainFrameTable.height(MainFrameHeight);
                this.SideLeft.height(MainFrameHeight);

        }


};


